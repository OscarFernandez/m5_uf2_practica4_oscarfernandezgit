import java.util.Scanner;

/**
 * <h2>La classe Calculadora, s'utilitza per a fer operacions matemtiques</h2>
 * 
 * @version 5-2021
 * @author ofernandez
 * @since 6-5-2021
 */

 //comentari desde gitlab

 
public class calculadora {

    static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int op1 = 0, op2 = 0;
		char opcio;
		int resultat = 0;
		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-':
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*':
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/':
                    if (control) {
                    	
                    	if (op2 == 0) {
                    		resultat = divideix(op1, op2);
                    	} else {
                        	resultat = divisio(op1, op2);
                    	}
                    	
                        if (resultat == -99)
                            System.out.print("No ha fet la divisió");
                        else
                             visualitzar(resultat);
					}
		            else mostrarError();
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opció erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}

	/**
	 * Mtode mostrarError
	 * Mostra un error si no s'introdueix primer els valors a operar
	 */
    public static void mostrarError() { /* procediment */
		System.out.println("\nError, cal introduir primer els valors a operar");
	}



    /**
	 * Mtode suma, amb 2 parmetres
	 * Sumar els 2 valors que nosaltres li donem
	 * @param a Valor 1
	 * @param b Valor 2
	 * @return Resultat
	 */
	public static int suma(int a, int b) { /* funció */
		int res;
		res = a + b;
		return res;
	}

	/**
	 * Mtode resta, amb 2 parmetres
	 * Restar els 2 valors que nosaltres li donem
	 * @param a Valor 1
	 * @param b Valor 2
	 * @return Resultat
	 */
	public static int resta(int a, int b) { /* funció */
		int res;
		res = a - b;
		return res;
	}

	/**
	 * Mtode multiplicacio, amb 2 parmetres
	 * Multiplicar els 2 valors que nosaltres li donem
	 * @param a Valor 1
	 * @param b Valor 2
	 * @return Resultat
	 */
	public static int multiplicacio(int a, int b) { /* funció */
		int res;
		res = a * b;
		return res;
	}

	/** OSCAR FDEZ
	 * Mtode divisio, amb 2 parmetres
	 * Dividir els 2 valors que nosaltres li donem
	 * @param a Valor 1
	 * @param b Valor 2
	 * @return Resultat
	 */
	public static int divisio(int a, int b) { /* funció */
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opció incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}

	/**
	 * Mtode llegirCar
	 * Llegir el carcter que nosaltres introduim
	 * @return Carcter
	 */
	public static char llegirCar() // funció
	{
		char car;

		System.out.print("Introdueix un carácter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}

	/**
	 * Comproba que el nmero introduit sigui enter
	 * @return <ul>
	 *               <li>true: el nombre s un enter</li>
	 *               <li>false: el nombre no s un enter</li>
	 *          </ul>
	 */
	public static int llegirEnter() // funció
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}

	/**
	 * Mtode visualitzar
	 * Indicar el resultat de l'operaci matemtica
	 * @param res Resultat
	 */
	public static void visualitzar(int res) { /* procediment */
		System.out.println("\nEl resultat de l'operacio és " + res);
	}
	
	/**
	 * Mtode mostrar_menu
	 * Mostrar un menu amb diferents opcions
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opció i ");
	}
	
	/** 	OSCAR FDEZ
	 * Mtode divideix, amb 2 parmetres
	 * Comprovar que la divisi es entre zero
	 * @param num1 Valor 1
	 * @param num2 Valor 2
	 * @return Resultat
	 */
	public static  int divideix(int num1, int num2) {
		int res;
	if (num2 == 0)
		throw new java.lang.ArithmeticException("Divisi entre zero");
	else 
	res = num1/num2;
		return res;
	}
}
